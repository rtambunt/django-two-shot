from django.db import models
from django.contrib.auth.models import User


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="categories"
    )


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="accounts"
    )


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(decimal_places=3, max_digits=10)
    tax = models.DecimalField(decimal_places=3, max_digits=10)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="receipts"
    )
    category = models.ForeignKey(
        ExpenseCategory, on_delete=models.CASCADE, related_name="receipts"
    )
    account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        related_name="receipts",
        blank=True,
        null=True,
    )
