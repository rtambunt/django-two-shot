from django.urls import path
from receipts.views import (
    receipts,
    create_receipt,
    category_list,
    create_expense_category,
    account_list,
    create_account,
)

urlpatterns = [
    path("", receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path(
        "categories/create/", create_expense_category, name="create_category"
    ),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
]
