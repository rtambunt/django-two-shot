from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt

# Register your models here.

# @admin.register(Account)

# @admin.register(ExpenseCategory)


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )


admin.site.register(Account)

admin.site.register(ExpenseCategory)
