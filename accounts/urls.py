from django.urls import path
from accounts.views import account_form, user_logout, user_signup

urlpatterns = [
    path("login/", account_form, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", user_signup, name="signup"),
]
